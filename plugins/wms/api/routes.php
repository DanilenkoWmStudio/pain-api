<?php

Route::group(['prefix' => 'api/v1'], function () {
    Route::resource('event', 'Wms\Api\Http\Event')->only(['index', 'store', 'show']);
    Route::resource('partner', 'Wms\Api\Http\Partner')->only(['index', 'create', 'show']);
});