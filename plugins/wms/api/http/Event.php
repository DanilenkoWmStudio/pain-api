<?php namespace Wms\Api\Http;

use Backend\Classes\Controller;

/**
 * Event Back-end Controller
 */
class Event extends Controller
{
    public $implement = [
      'Mohsin.Rest.Behaviors.RestController',
    ];

    public $restConfig = 'config_rest.yaml';

    public function index()
    {
        $paginate = post('paginate') ?: 2;

        return \Wms\School\Models\Event::orderByDesc('id')->paginate($paginate);
    }

    public function show($id)
    {
        return \Wms\School\Models\Event::with('image')->find($id);
    }

}
