<?php namespace Wms\Api\Http;

use Backend\Classes\Controller;

/**
 * Partner Back-end Controller
 */
class Partner extends Controller
{
    public $implement = [
        'Mohsin.Rest.Behaviors.RestController'
    ];

    public $restConfig = 'config_rest.yaml';

}
