<?php namespace Wms\School;

use Backend;
use System\Classes\PluginBase;

/**
 * school Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'school',
            'description' => 'No description provided yet...',
            'author'      => 'wms',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Wms\School\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'wms.school.some_permission' => [
                'tab' => 'school',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {

        return [
            'school' => [
                'label'       => 'api',
                'url'         => Backend::url('wms/school/events'),
                'icon'        => 'icon-leaf',
                'permissions' => ['wms.school.*'],
                'order'       => 500,
                'sideMenu' => [
                    'events' => [
                          'label' => 'Новости',
                          'url' => Backend::url('wms/school/events'),
                          'icon' => 'icon-newspaper-o',
                          'permissions' => ['wms.school.moderator'],
                          'order' => 400
                    ],
                  'partners' => [
                    'label' => 'Партнеры',
                    'url' => Backend::url('wms/school/partners'),
                    'icon' => 'icon-briefcase',
                    'permissions' => ['wms.school.moderator'],
                    'order' => 400
                  ],

                ]
            ],
        ];
    }
}
