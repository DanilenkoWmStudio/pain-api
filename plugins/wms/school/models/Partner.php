<?php namespace Wms\School\Models;

use Model;
use System\Models\File;

/**
 * Partner Model
 */
class Partner extends Model {
    /**
     * @var string The database table used by the model.
     */
    public $table = 'wms_school_partners';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = ['school' => 'Wms\School\Models\School'];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = ['image' => File::class];
    public $attachMany = [];
}
