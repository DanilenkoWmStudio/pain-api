<?php namespace Wms\School\Models;

use Model;
use October\Rain\Database\Traits\Sluggable;
use System\Models\File;

/**
 * Event Model
 */
class Event extends Model {

    use Sluggable;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'wms_school_events';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['id'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = ['image' => File::class];
    public $attachMany = [];

    protected $slugs = ['slug' => 'title'];

    public function beforeSave() {
        $this->slug = null;
        $this->slugAttributes();
    }
}
