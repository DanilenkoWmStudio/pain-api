<?php namespace Wms\School\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Partners Back-end Controller
 */
class Partners extends Controller {
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $requiredPermissions = [
        'wms.school.moderator'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct() {
        parent::__construct();

        BackendMenu::setContext('Wms.School', 'school', 'partners');
    }


}
